const sqlite = require('sqlite-sync');
sqlite.connect('hdiDatabase.db');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const authConfig = require('../config/auth');

function generateToken(params = {}){
    return jwt.sign(params, authConfig.secret,{
        expiresIn: 7200
    });
}

module.exports = {

    async create(req, res){
        try {
            let user = sqlite.run("SELECT * FROM USER WHERE CPFCNPJ LIKE ? ",[req.body.cpfCnpj]);
            if ( user.length !== 0) {
                return res.status(400).send({ error: 'Email Já Cadastrado'});
            } else {
                user = sqlite.insert('USER',{CPFCNPJ: req.body.cpfCnpj, PASSWORD: req.body.password});
                return res.json(user);
            }
        }catch (error) {
            return res.status(400).send({error: 'Erro Ao Criar Usuario'})
        }
    },

    async index(req, res){
        try{
            const users = await sqlite.run("SELECT ID,CPFCNPJ FROM USER");
            res.json(users);
        } catch (error) {
            return res.sendStatus(400, ).send({error: 'Erro ao Buscar Usuarios'})
        }
    },

    async show(req, res){
        try{
            const users = await sqlite.run("SELECT ID,CPFCNPJ FROM USER WHERE CPFCNPJ LIKE ? ",[req.params.cpfCnpj]);
            res.json(users[0]);
        } catch (error) {
            return res.sendStatus(400, ).send({error: 'Erro ao Buscar Usuarios'})
        }
    },

    async login(req, res) {
        const {cpfCnpj, password, akamaiBot} = req.body;
        try {
            let user = sqlite.run("SELECT * FROM USER WHERE CPFCNPJ LIKE ? AND PASSWORD LIKE ?", [cpfCnpj, password]);

            if (user.length === 0) {
                return res.status(400).send({error: 'Usuario ou senha incorreto'});
            } else if (akamaiBot !== ""){
                res.send({token: generateToken({id: user.id, cpfCnpj: user.cpfCnpj}), akamaiBot: true});
            } else {
                res.send({token: generateToken({id: user.id, cpfCnpj: user.cpfCnpj}), akamaiBot: false});
            }
        } catch (error) {
            return res.status(400).send({error: 'Login Error'})
        }
    },
};
