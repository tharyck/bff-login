const express = require('express');
const router = express.Router();
const controller = require('../bin/controllers/UserController');

router.post('/',  controller.login);

module.exports = router;
