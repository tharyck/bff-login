const express = require('express');
const router = express.Router();
const controller = require('../bin/controllers/UserController');
const auth = require('../bin/middlewares/auth');

// router.use(auth);
router.get('/',  controller.index);
router.post('/', controller.create);
router.get('/:cpfCnpj', controller.show);
module.exports = router;
