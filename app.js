var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/UsersRouter');
var logginRoute = require('./routes/LoginRouter');
const sqlite = require('sqlite-sync');

sqlite.connect('hdiDatabase.db');

//Criando tabela
sqlite.run("CREATE TABLE USER(ID INTEGER PRIMARY KEY AUTOINCREMENT, CPFCNPJ TEXT NOT NULL, PASSWORD TEXT);");

//Inserindo dados
// const pessoaFisica = sqlite.insert('USER',{ID: 1, CPFCNPJ: "54958640002", PASSWORD: "TESTEHDI"});
// console.log(pessoaFisica)

// const pessoaJuridica = sqlite.insert('USER',{ID: 2, CPFCNPJ: "82848188000197", PASSWORD: "TESTEHDI"});
// console.log(pessoaJuridica)

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/api/users', usersRouter);
app.use('/api/login', logginRoute);

module.exports = app;
